import java.util.Arrays;
import java.util.Scanner;

public class main {


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Please input the word : ");
        System.out.println(AlphabetSoup(in.nextLine()));
    }

    public static String AlphabetSoup(String str){
            String whatIts = str;
            String regexSelector = "[^a-z]";
            whatIts = whatIts.replaceAll(regexSelector,"");
            String[] newFiltered = whatIts.split("");
            Arrays.sort(newFiltered);
            StringBuilder ret = new StringBuilder();
            for (String s : newFiltered) {
                ret.append(s);
            }
            return ret.toString();
    }
}
